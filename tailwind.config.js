module.exports = {
  purge: [],
  theme: {
    extend: {
      colors: {
        'blue': '#1DA1F2',
        'darkblue': '#1A91DA',
        'lightblue': '#EFF9FF',
        'dark': '#657786',
        'light': '#AABBC2',
        'lighter': '#E1E8ED',
        'lightest': '#F5F8FA',
        'red': '#FF0000',
      }
    },

  },
  variants: {},
  plugins: [],
}